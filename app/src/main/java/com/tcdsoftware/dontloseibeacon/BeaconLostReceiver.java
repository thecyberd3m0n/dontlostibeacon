package com.tcdsoftware.dontloseibeacon;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BeaconLostReceiver extends BroadcastReceiver  {
    final static String ACTION_BEACON_LOST = "android.intent.action.BEACON_LOST";
    final static String PACKAGE_NAME = "com.tcdsoftware.dontloseibeacon";

    public BeaconLostReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_BEACON_LOST)) {
//            Intent notifyIntent = new Intent(this, BeaconLostActivity.class);
            beaconLost(context, intent);
        } else {
            Log.d("BeaconLostReceiver", "Unhandled Action received: " + intent.getAction());
        }
    }

    private void beaconLost(Context context, Intent intent) {
        Log.v("BeaconLostReceiver", "Starting BeaconLost activity");


    }
}
