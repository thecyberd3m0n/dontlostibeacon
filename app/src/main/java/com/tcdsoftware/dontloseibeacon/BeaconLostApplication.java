package com.tcdsoftware.dontloseibeacon;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

public class BeaconLostApplication extends Application {
    private static class LifecycleObserver implements DefaultLifecycleObserver {
        private boolean inForeground = false;

        @Override
        public void onStart(@NonNull LifecycleOwner owner) {
            inForeground = true;
        }

        @Override
        public void onStop(@NonNull LifecycleOwner owner) {
            inForeground = false;
        }

        boolean isInForeground() {
            return inForeground;
        }
    }
    private static final LifecycleObserver foregroundTracker = new LifecycleObserver();

    @Override
    public void onCreate() {
        super.onCreate();

    }

    public static boolean isInForeground() {
        return foregroundTracker.isInForeground();
    }
}
