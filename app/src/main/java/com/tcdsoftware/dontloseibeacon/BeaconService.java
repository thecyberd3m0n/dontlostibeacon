package com.tcdsoftware.dontloseibeacon;

import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;
import static androidx.core.app.NotificationCompat.PRIORITY_HIGH;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;
import java.util.function.Consumer;

public class BeaconService extends Service implements MonitorNotifier, RangeNotifier {
    public static final String EXPECTED_DEVICE_ADDRESS = "D0:F0:18:78:07:0F";
    private static final String TAG = ".BackgroundService";
    private PowerManager powerManager;
    private final int defaultNotificationId = 1337;
    private final int lostNotificationId = 1338;
    private NotificationManager notificationManager;
    private boolean watching = false;
    private Runnable finalizer = null;
    private Handler timeoutHandler = null;
    private NotificationCompat.Builder defaultNotificationBuilder = null;


    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NotificationHelper.initializeChannels(this);

        this.defaultNotificationBuilder = new NotificationCompat.Builder(this, NotificationHelper.Channels.WATCHING_BEACONS);
        Notification notification = this.defaultNotificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(PRIORITY_DEFAULT)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Beacon Scanner")
                .setContentText("Watching your beacons")
                .build();

        notificationManager = ContextCompat.getSystemService(this, NotificationManager.class);
        powerManager = ContextCompat.getSystemService(this, PowerManager.class);
        startScanningBeacons(notification);

    }

    private void startScanningBeacons(Notification notification) {
        Log.v("BeaconService", "Scanning beacons");
        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.enableForegroundServiceScanning(notification, defaultNotificationId);
        setScanningPeriod(15000);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        Region region = new Region("com.tcdsoftware.dontloseibeacon.BeaconService", null, null, null);
        BeaconManager.getInstanceForApplication(this).startMonitoring(region);
        BeaconManager.getInstanceForApplication(this).addMonitorNotifier(this);
        BeaconManager.getInstanceForApplication(this).addRangeNotifier(this);
        BeaconManager.getInstanceForApplication(this).startRangingBeacons(region);
    }

    @Override
    public void didEnterRegion(Region arg0) {
        Log.d(TAG, "Got a didEnterRegion call");
        Log.d(TAG, arg0.getUniqueId());
    }

    @Override
    public void didExitRegion(Region arg0) {
        Log.d(TAG, "Got a didExitRegion call");
        Log.d(TAG, arg0.getUniqueId());

        // start beacon lost activity intent

    }

    private void showBroadcastNotification() {
        Intent intent = new Intent(this, BeaconLostReceiver.class);
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
//        intent.setAction(BeaconLostReceiver.ACTION_BEACON_LOST);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        createNotifcation(pendingIntent);
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    private void showActivityNotification() {
        Intent intent = new Intent(this, BeaconLostActivity.class);

        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_MUTABLE);
        createNotifcation(pi);
    }

    private void createNotifcation(PendingIntent pendingIntent) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        NotificationChannel mChannel = null;
        notification
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setOngoing(false)
                .setPriority(PRIORITY_HIGH)
                .setContentTitle(this.getString(R.string.beacon_lost))
                .setOnlyAlertOnce(true)
                .setCategory(Notification.CATEGORY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(NotificationHelper.Channels.BEACON_LOST, NotificationHelper.Channels.BEACON_LOST, NotificationManager.IMPORTANCE_HIGH);
            notification.setChannelId(NotificationHelper.Channels.BEACON_LOST);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }
        notification.setGroup(NotificationHelper.Channels.BEACON_LOST);
        notification.setChannelId(NotificationHelper.Channels.BEACON_LOST);

        notificationManager.notify(lostNotificationId, notification.build());
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        Log.d(TAG, "Got a didRangeBeaconsInRegion call");
        // TODO: dedupe collection by ID
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);

        Log.d(TAG, "Got a didRangeBeaconsInRegion call");
        Log.d(TAG, "beacons");
        Log.d(TAG, String.valueOf(beacons.size()));
        final boolean[] beaconFound = {false};
        beacons.forEach(new Consumer<Beacon>() {
            @Override
            public void accept(Beacon beacon) {
                if (beacon == null) return;
                if (beacon.getBluetoothAddress().equals(EXPECTED_DEVICE_ADDRESS)) {
                    beaconFound[0] = true;
                    Log.d(TAG, "Beacon found. Address: ");
                    Log.d(TAG, beacon.getBluetoothAddress());
                    Log.d(TAG, "RSSI:");
                    BeaconService.this.defaultNotificationBuilder.setContentText("Watching you beacon, RSSI: " + String.valueOf(beacon.getRssi()));
                    BeaconService.this.notificationManager.notify(defaultNotificationId, defaultNotificationBuilder.build());
                    Log.d(TAG, String.valueOf(beacon.getRssi()));
                }
            }
        });
        if (!watching && beaconFound[0]) {
            watching = true;
        } else if (watching && beaconFound[0]) {
            if (this.timeoutHandler != null) {
                Log.d(TAG, "Beacon found. Canceled timeout callbacks");
                this.setScanningPeriod(15000);
                this.timeoutHandler.removeCallbacks(this.finalizer);
                this.timeoutHandler = null;
            }
        } else if (watching) {
            Log.d(TAG, "Beacon NOT found. Enabling callback");
            if (timeoutHandler == null) {
                this.timeoutHandler = new Handler(Looper.getMainLooper());
                this.setScanningPeriod(5000);
                this.finalizer = new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.S)
                    public void run() {
                        BeaconService.this.alarm();
                        BeaconService.this.watching = false;
                        BeaconService.this.timeoutHandler = null;
                        BeaconService.this.defaultNotificationBuilder.setContentText("LOST!! Awaiting beacon to be found");
                        BeaconService.this.notificationManager.notify(defaultNotificationId, defaultNotificationBuilder.build());
                        BeaconService.this.setScanningPeriod(15000);

                    }
                };
                timeoutHandler.postDelayed(finalizer, 35000);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    private void alarm() {
        if (Build.VERSION.SDK_INT < 29 || BeaconLostApplication.isInForeground()) {
            Intent intent = new Intent();
            Intent startActivity = new Intent(this, BeaconLostActivity.class);
            startActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else {
            if (powerManager.isInteractive()) {
                showBroadcastNotification();

            } else {
                showActivityNotification();

            }
        }
    }

    private void setScanningPeriod(int period) {
        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.setForegroundBetweenScanPeriod(period);
        beaconManager.setBackgroundBetweenScanPeriod(period);
    }
}